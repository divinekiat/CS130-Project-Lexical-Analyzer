### HOW TO RUN
______________________________

##### Compile:
```
javac -cp ./junit-4.8.2.jar:. *.java && javac */*.java
```

##### Run Unit Test:
```
java -cp ./junit-4.8.2.jar:. org.junit.runner.JUnitCore UnitTesting
```

##### Run File Test:
```
java LexicalAnalyzer.LexicalAnalyzer tab1.html > program_tab1out.txt
java LexicalAnalyzer.LexicalAnalyzer tab2.html > program_tab2out.txt
java LexicalAnalyzer.LexicalAnalyzer manycases.txt > program_mcout.txt
java DescentParser.DescentParser tab1.html > program_tab1out.csv
java FileTest
```

##### Run Lexical Analyzer:
```
java LexicalAnalyzer.LexicalAnalyzerLexicalAnalyzer <input_file> 
```

##### Run Descent Parser:
```
java DescentParser.DescentParser <input_file> <output_file>
```

_______________________________