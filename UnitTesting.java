import org.junit.*;
import LexicalAnalyzer.*;

public class UnitTesting{

	@Test
	public void TestIdent() {
		Program program = new Program("sss");
		Token correct_token = new Token(Token.Id.IDENT, "sss");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber() {
		Program program = new Program("123");
		Token correct_token = new Token(Token.Id.NUMBER, "123");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber2() {
		Program program = new Program("123.");
		Token correct_token = new Token(Token.Id.NUMBER, "123.");
		correct_token.setError("***lexical error: badly formed number\n");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber3() {
		Program program = new Program("123.0");
		Token correct_token = new Token(Token.Id.NUMBER, "123.0");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber4() {
		Program program = new Program("123.e");
		Token correct_token = new Token(Token.Id.NUMBER, "123.e");
		correct_token.setError("***lexical error: badly formed number\n");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber5() {
		Program program = new Program("123.0e");
		Token correct_token = new Token(Token.Id.NUMBER, "123.0e");
		correct_token.setError("***lexical error: badly formed number\n");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber6() {
		Program program = new Program("123.0e-");
		Token correct_token = new Token(Token.Id.NUMBER, "123.0e-");
		correct_token.setError("***lexical error: badly formed number\n");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber7() {
		Program program = new Program("123.0e-1");
		Token correct_token = new Token(Token.Id.NUMBER, "123.0e-1");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber8() {
		Program program = new Program("123.0e1");
		Token correct_token = new Token(Token.Id.NUMBER, "123.0e1");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber9() {
		Program program = new Program("123..");
		Token correct_token = new Token(Token.Id.NUMBER, "123..");
		correct_token.setError("***lexical error: badly formed number\n");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber10() {
		Program program = new Program("123.0.");
		Token correct_token = new Token(Token.Id.NUMBER, "123.0");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber11() {
		Program program = new Program("123.0e1e");
		Token correct_token = new Token(Token.Id.NUMBER, "123.0e1");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestNumber12() {
		Program program = new Program("123.0ee");
		Token correct_token = new Token(Token.Id.NUMBER, "123.0ee");
		correct_token.setError("***lexical error: badly formed number\n");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestIllegal() {
		Program program = new Program("!");
		Token correct_token = new Token(null, "");
		correct_token.setError("***lexical error: illegal character (!)");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestIllegal1() {
		Program program = new Program("$");
		Token correct_token = new Token(null, "");
		correct_token.setError("***lexical error: illegal character ($)");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestPlus() {
		Program program = new Program("+");
		Token correct_token = new Token(Token.Id.PLUS, "+");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}


	@Test
	public void TestMinus() {
		Program program = new Program("-");
		Token correct_token = new Token(Token.Id.MINUS, "-");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestMult() {
		Program program = new Program("*");
		Token correct_token = new Token(Token.Id.MULT, "*");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestDivide() {
		Program program = new Program("/");
		Token correct_token = new Token(Token.Id.DIVIDE, "/");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestExp() {
		Program program = new Program("**");
		Token correct_token = new Token(Token.Id.EXP, "**");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestLParen() {
		Program program = new Program("(");
		Token correct_token = new Token(Token.Id.LPAREN, "(");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestRParen() {
		Program program = new Program(")");
		Token correct_token = new Token(Token.Id.RPAREN, ")");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestEquals() {
		Program program = new Program("=");
		Token correct_token = new Token(Token.Id.EQUALS, "=");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestLThan() {
		Program program = new Program("<");
		Token correct_token = new Token(Token.Id.LTHAN, "<");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestGThan() {
		Program program = new Program(">");
		Token correct_token = new Token(Token.Id.GTHAN, ">");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestColon() {
		Program program = new Program(":");
		Token correct_token = new Token(Token.Id.COLON, ":");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestComma() {
		Program program = new Program(",");
		Token correct_token = new Token(Token.Id.COMMA, ",");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestSColon() {
		Program program = new Program(";");
		Token correct_token = new Token(Token.Id.SCOLON, ";");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestPeriod() {
		Program program = new Program(".");
		Token correct_token = new Token(Token.Id.PERIOD, ".");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestQuote() {
		Program program = new Program("\'");
		Token correct_token = new Token(Token.Id.QUOTE, "\'");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestDQuote() {
		Program program = new Program("\"");
		Token correct_token = new Token(Token.Id.DQUOTE, "\"");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestEndTagHead() {
		Program program = new Program("</");
		Token correct_token = new Token(Token.Id.ENDTAGHEAD, "</");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestTagIdent() {
		Program program = new Program("<abc");
		Token correct_token = new Token(Token.Id.TAGIDENT, "<abc");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestComment() {
		Program program = new Program("<!-- c -->");
		Token correct_token = new Token(Token.Id.COMMENT, "");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test
	public void TestComment1() {
		Program program = new Program("<!- c -->");
		Token correct_token = new Token(Token.Id.LTHAN, "<");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void TestComment2() {
		Program program = new Program("<!--");
		program.getToken(); //should return unexpected EOF
	}


	@Test(expected = IndexOutOfBoundsException.class)
	public void TestComment3() {
		Program program = new Program("<!-- -");
		program.getToken(); //should return unexpected EOF
	}


	@Test(expected = IndexOutOfBoundsException.class)
	public void TestComment4() {
		Program program = new Program("<!-- --");
		program.getToken(); //should return unexpected EOF
	}


	@Test(expected = IndexOutOfBoundsException.class)
	public void TestComment5() {
		Program program = new Program("<!-- -- c ");
		program.getToken(); //should return unexpected EOF
	}

	@Test
	public void TestComment6() {
		Program program = new Program("<!-- -- c -->");
		Token correct_token = new Token(Token.Id.COMMENT, "");

		Assert.assertEquals(true, program.getToken().isSame(correct_token));
	}

}