import java.io.File;
import java.util.Scanner;
import LexicalAnalyzer.*;

public class FileTest {

	public static void main(String args[]) throws java.io.FileNotFoundException, java.io.IOException{
		compareFiles("program_tab1out.txt", "tab1out.txt");
		compareFiles("program_tab2out.txt", "tab2out.txt");
		compareFiles("program_mcout.txt", "mcout.txt");
	}

	public static void compareFiles(String ProgramOutputFilepath, String CorrectTestOutputFilepath) throws java.io.FileNotFoundException{

		String testContent = new Scanner(new File(CorrectTestOutputFilepath)).useDelimiter("\\Z").next();
		String programContent = new Scanner(new File(ProgramOutputFilepath)).useDelimiter("\\Z").next();
		
		if(!testContent.equals(programContent)){
		    System.out.println("TEST " + ProgramOutputFilepath +":  FAILED");
			throw new RuntimeException();
		}
		
		System.out.println("TEST " + ProgramOutputFilepath +": SUCCESS");
	}
}