package DescentParser;

// PEMDAS Grammar
// S -> T | S + T | S - T
// T -> F | T * F | T / F | T % Fgit
// F -> +F | -F | (S) | n | F ** F

public class Calculator {
	String s; // input string
	int offset; // offset of the string
	int ca; // character in ASCII

	public Calculator(String s) {
		this.s = s;
		this.offset = -1; // so next is 0
	}

	void incrementOffset() {
		if(++offset < s.length()) {
			ca = s.charAt(offset);
		} else
			ca = -1;
	}

	boolean isCharacter(int c) {
		while(ca == ' ')	// skips white space
			incrementOffset();
		if(ca == c) {
			incrementOffset();
			return true;
		}
		return false;
	}

	double calculate() {
		incrementOffset();
		double s = S();
		return s;
	}

	double S() {
		double t = T();
		while(true) {
			if(isCharacter('+'))
				t += T();
			else if(isCharacter('-'))
				t -= T();
			else
				return t;
		}
	}

	double T() {
		double f = F();
		while(true) {
			if(isCharacter('*')) {
				f *= F();
			}
			else if(isCharacter('/'))
				f /= F();
			else if(isCharacter('%'))
				f %= F();
			else
				return f;
		}
	}

	double F() {
		if(isCharacter('+'))	// positive
			return F();
		if(isCharacter('-'))	// negative
			return -F();

		double n;
		int startOffset = this.offset;

		if(isCharacter('(')) {
			n = S();
			isCharacter(')');
		} else if((ca >= '0' && ca <= '9') || ca == '.') {
			while((ca >= '0' && ca <= '9') || ca == '.' || ca == 'E' || ca == 'e') {
				if(ca == 'E' || ca == 'e') {
					incrementOffset();
					if(ca == '-') {
            			incrementOffset();
            			while(ca >= '0' && ca <= '9')
            				incrementOffset();
            		} else {
            			while(ca >= '0' && ca <= '9')
            				incrementOffset();
            		}
            	} else {
            		incrementOffset();
				}
			}
			n = Double.parseDouble(s.substring(startOffset, this.offset));
		} else {
			throw new RuntimeException("Wrong factor format: " + (char)ca);
		}

		if(isCharacter('*')) {
			if(isCharacter('*')) {
				n = Math.pow(n, F());
			} else {
				n *= F();
			}
		}

		return n;
	}

	public static void main(String args[]) {
	 	Calculator c = new Calculator("5%2");
	 	System.out.println(c.calculate());
	}
}