package DescentParser;

//TESTING IF CSVWRITER WORKS
public class CSVTest {
	public static void main(String args[]) throws java.io.IOException {
		CSVWriter a = new CSVWriter("test.csv");

		// a.addEntry("a");
		// a.addEntry("“"); //FOR QUOTATIONS USE “
		// a.addEntry(",,,,");
		// a.addEntry("d");
		// a.writeRow();

		// a.addEntry("1");
		// a.addEntry("2");
		// a.addEntry("3");
		// a.writeRow();

		// a.addEntry("fnadas");
		// a.addEntry("asd");
		// a.writeRow();

		//PARSER SHOULD FOLLOW ORDER
		a.addEntry("particulars");
		a.addEntry("amount");
		a.writeRow();

		a.addEntry("ballpen");
		a.addEntry("10.25");
		a.writeRow();

		a.addEntry("pencil");
		a.addEntry("10.25");
		a.writeRow();

		a.addEntry("total amount");
		a.addEntry("21.35");
		a.writeRow();


		a.addEntry("formula used");
		a.addEntry("10.25+2*5.55");
		a.writeRow();

		a.write();
	}
}