package DescentParser;

import java.io.FileWriter;
import java.util.ArrayList;

public class CSVWriter{
  private FileWriter pw;
  private String path;
  private ArrayList<String> data = new ArrayList<>();

  public CSVWriter(String outputFilePath) {
    path = outputFilePath;
    initialize();
  }

  //OPENING OR CREATING NEW FILES
  public boolean initialize() {
    try {
      //IF FILE EXISTS JUST APPEND TO NEW FILE
      pw = new FileWriter(path,true); 
      return true;

    } catch (Exception e) {
        System.out.println("Can't create file.");
        e.printStackTrace();
        return false;

    }
  }
  //WHEN ANALYZER READS <TD>
  public void writeRow() throws java.io.IOException{
    StringBuilder sb = new StringBuilder();
    String prefix = "";
    for (String s : data) {
       sb.append(prefix);
       prefix = "\",\"";
       sb.append(s);
    }
    //System.out.println("\"" + sb.toString() + "\"");
    pw.append("\"" + sb.toString() + "\"");
    pw.append("\n");

    data = new ArrayList<>();
  }

  //WHEN ANALYZER READS <TR>
  public void addEntry(String s) {
    data.add(s);
  }

  //FLUSH CHANGES
  public void write() throws java.io.IOException{
    pw.flush();
    pw.close();
  }
}