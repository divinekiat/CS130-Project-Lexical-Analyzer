package DescentParser;

import LexicalAnalyzer.*;

// Grammar: 
//		S -> <table>X</table>  (One table only!)
// 		X -> <tr>Y</tr> || <tr>Y</tr>X
// 		Y -> <td>in</td> || <th>in</th> || <td>in</td>Y || <th>in</th>Y
//		in -> ident || expr 
// 		ident -> string of ident numbers and symbols
// 		expr -> starts with =, accepts numbers and operands only after =
//		string_expr -> [expr]

public class DescentParser {
	static Program program;
	static Token t;
	static CSVWriter csvWriter;
	static String entry = "";
	static boolean inDQuote = false, inQuote = false, inBracket = false, isStringExp = false;

	public static void main(String args[]) throws java.io.FileNotFoundException, java.io.IOException {
		program = new Program(args[0]); //LexicalAnalyzer.Program
		csvWriter = new CSVWriter(args[1]);
		getNextToken();
		S();
	}

	public static void S() throws java.io.IOException {
		try {
			tableHead();
			row();
			tableEndHead();
			csvWriter.write();
			finish();
		} catch (RuntimeException e) {
			System.out.println("Grammar not accepted.");
			System.exit(0);
		}
	}

	public static void row() throws java.io.IOException {
		try {
			trHead();
			entry();
			trEnd();
			csvWriter.writeRow();
			row();
		} catch (RuntimeException e) {
			return;
		}
	}

	public static void entry() throws java.io.IOException {
		try {
			entryHead();
			input();
			entryEnd();
			csvWriter.addEntry(entry);
			entry = "";
			entry();
		} catch (RuntimeException e) {
			return;
		}
	}

	public static void expression() throws java.io.IOException {
		while(t.getId()!= Token.Id.ENDTAGHEAD) {
			getNextToken();
			if(t.getId()!= Token.Id.ENDTAGHEAD) entry = entry + t.getLexeme();
		} 
		try {
			Calculator c = new Calculator(entry);
			entry = c.calculate() + "";
		} catch(Exception e) {
			end();
		}
		return;
	}

	public static void bracket() throws java.io.IOException {
		if(!inBracket && !isStringExp) {
			inBracket = true;
			isStringExp = true;
			bracketIn();
			try {
				Calculator c = new Calculator(entry);
				c.calculate();
			} catch (RuntimeException e){
				end();
			}
			getNextToken();
		}
		else end();
	}

	public static void bracketIn() throws java.io.IOException {
		getNextToken();
		if(t.getId()!= Token.Id.RBRACK) {
			entry = entry + t.getLexeme();
			bracketIn();
		}
		else {
			inBracket = false;
			return;
		}

	}

	public static void ident() throws java.io.IOException {
		entry = entry + " " + t.getLexeme();
		getNextToken();
		if(t.getId() == Token.Id.IDENT || t.getId() == Token.Id.NUMBER) ident();
		else return;
	}

	public static void symbol() throws java.io.IOException {
		switch (t.getId()) {
			case DQUOTE:
				getNextToken();				
				if(t.getId() == Token.Id.DQUOTE) {
					if(!inDQuote) entry = entry + " “”";
					else entry = entry + "” “";
					getNextToken();
				}
				else if(!inDQuote) {
					entry = entry + " “";
					if(t.getId() == Token.Id.IDENT || t.getId() == Token.Id.NUMBER) {
						entry = entry + t.getLexeme();
						getNextToken();
					} else if (t.getId() == Token.Id.QUOTE){
							entry = entry + "'";
							inQuote = !inQuote;
							getNextToken();
							if(t.getId() == Token.Id.IDENT || t.getId() == Token.Id.NUMBER) {
								entry = entry + t.getLexeme();
								getNextToken();
							}
						}
					inDQuote = true;
	
				} else {
					entry = entry + "”";
					inDQuote = !inDQuote;
				}
				break;
			case QUOTE:
				getNextToken();
				if(t.getId() == Token.Id.QUOTE) {
					if(!inQuote) entry = entry + " \'\'";
					else entry = entry + "\' \'";
					getNextToken();
				}
				else if(!inQuote) {
					if(t.getLexeme().equals("s")) {
						entry = entry +"\'" + t.getLexeme();
						inQuote = false;
						getNextToken();
					}else {
						entry = entry + " " + "\'";
						if(t.getId() == Token.Id.IDENT || t.getId() == Token.Id.NUMBER) {
							entry = entry + t.getLexeme();
							getNextToken();
						} else if (t.getId() == Token.Id.DQUOTE){
							if(!inDQuote) entry = entry + "“";
							else entry = entry + "”";
							inDQuote = !inDQuote;
							getNextToken();
							if(t.getId() == Token.Id.IDENT || t.getId() == Token.Id.NUMBER) {
								entry = entry + t.getLexeme();
								getNextToken();
							}
						}
						inQuote = true;
					} 
				} else {
					entry = entry + "\'";
					inQuote = !inQuote;
				}
				break;
			default:
				entry = entry + t.getLexeme();
				getNextToken();
		}
		return;
	}

	public static void input() throws java.io.IOException {
		if(t.getId() != Token.Id.ENDTAGHEAD){
			if(entry.equals("")) {
				if(t.getId() == Token.Id.EQUALS) expression();
				else if (t.getId() == Token.Id.LBRACK) bracket();
				else if (t.getId() == Token.Id.DQUOTE) {
					entry = entry + "“"; 
					inDQuote = !inDQuote;
					getNextToken();
					entry = entry + t.getLexeme();
					getNextToken();
				} 
				else if (t.getId() == Token.Id.QUOTE) {
					entry = entry + "\'"; 
					inQuote = !inQuote;
					getNextToken();
					entry = entry + t.getLexeme();
					getNextToken();
				} 
				else {
					entry = entry + t.getLexeme();
					getNextToken();
				}
			} else {
				if(isStringExp) end();
				else {
					if(t.getId() == Token.Id.IDENT || t.getId() == Token.Id.NUMBER) ident();
					else symbol();
				}
			}
			input();
		} else {
			inBracket = false;
			isStringExp = false;
			return;
		}
	}

	public static void entryHead() throws java.io.IOException {
		entryIdent();
		gThan();
	}

	public static void entryIdent() throws java.io.IOException {
		if(t.getId() == Token.Id.TAGIDENT) {
			if(t.getLexeme().equals("<th") || t.getLexeme().equals("<td")){
				getNextToken();
				return;
			}
		}
		else end();
	}

	public static void entryEnd() throws java.io.IOException {
		endTagHead();
		identEntry();
		gThan();
	}

	public static void identEntry() throws java.io.IOException {
		if(t.getId() == Token.Id.IDENT) {
			if(t.getLexeme().equals("th") || t.getLexeme().equals("td")){
				getNextToken();
				return;
			}
		}
		else end();
	}

	public static void trHead() throws java.io.IOException {
		trIdent();
		gThan();
	}

	public static void trIdent() throws java.io.IOException {
		if(t.getId() == Token.Id.TAGIDENT) {
			if(t.getLexeme().equals("<tr")){
				getNextToken();
				return;
			}
		}
		else end();
	}

	public static void trEnd() throws java.io.IOException {
		endTagHead();
		identTr();
		gThan();
	}

	public static void identTr() throws java.io.IOException {
		if(t.getId() == Token.Id.IDENT) {
			if(t.getLexeme().equals("tr")){
				getNextToken();
				return;
			}
		}
		else end();
	}

	public static void tableHead() throws java.io.IOException {
		tableIdent();
		gThan();
	}

	public static void tableIdent() throws java.io.IOException {
		if(t.getId() == Token.Id.TAGIDENT) {
			if(t.getLexeme().equals("<table")){
				getNextToken();
				return;
			}
		}
		else end();
	}

	public static void gThan() throws java.io.IOException {
		if(t.getId() == Token.Id.GTHAN){
			getNextToken();
			return;
		} else {
			end();
		}
	}

	public static void tableEndHead() throws java.io.IOException {
		endTagHead();
		identTable();
		gThan();
	}

	public static void identTable() throws java.io.IOException {
		if(t.getId() == Token.Id.IDENT) {
			if(t.getLexeme().equals("table")){
				getNextToken();
				return;
			}
		}
		else end();
	}

	public static void endTagHead() throws java.io.IOException {
		if(t.getId() == Token.Id.ENDTAGHEAD){
			getNextToken();
			return;
		} else {
			end();
		}
	}

	public static void end() throws java.io.IOException {
		throw new RuntimeException();
	}

	public static void finish() throws java.io.IOException {
		_checkEnd();
		throw new RuntimeException();
	}

	public static void getNextToken() throws java.io.IOException {
		t = _getNextToken();
		_checkEnd();
	}

	private static Token _getNextToken(){
		try {
			Token t = program.getToken(); //Note: throws error at EOF
			//Return null if error in tokenization
			if (!t.getError().equals("")) {
				System.out.println(t.getError());
				return null;	
			}
			//Get next token if comment
			else if (t.getId() == Token.Id.COMMENT) {
				return program.getToken();
			}
			return t;

		} catch (Exception e) {
			//If not in start state at EOF, it means that there is un-expected EOF
    		if(!program.getContext().getState().toString().equals("Start State")) System.out.println("***lexical error: un-expected end of file");
    		return null;
		}
	}

	private static void _checkEnd() throws java.io.IOException {
		if (t == null){
			csvWriter.write();
			System.out.println("Stopped Parsing.");
			System.exit(0);
		}
	}
}