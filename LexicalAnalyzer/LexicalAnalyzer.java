package LexicalAnalyzer;

// driver program
public class LexicalAnalyzer {
	public static void main(String args[]) throws java.io.FileNotFoundException, java.io.IOException {
		Program program = new Program(args[0]);
		String format = "%s%-11s%s%n";
		//System.out.printf(format, "TOKEN", "LEXEME");
		//System.out.println("--------------------");

		Token t = program.getToken();
		try {
			while (t.getId() != Token.Id.EOF) {
				if (t.getId() == null) {
					System.out.println(t.getError());
				}
				else if (t.getId() != Token.Id.COMMENT) {
					System.out.printf(format, t.getError(), t.getId(), t.getLexeme());
				}

				t = program.getToken();
			}
		} catch (Exception e) {
      		if (program.context.getTokenId()!= null || !program.context.getLexeme().equals("")) System.out.println("***lexical error: un-expected end of file");
      		else t.setId(Token.Id.EOF);
		}
	}
}