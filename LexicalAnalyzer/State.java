package LexicalAnalyzer;

public interface State {
   public boolean parse(Context context, String s);
}