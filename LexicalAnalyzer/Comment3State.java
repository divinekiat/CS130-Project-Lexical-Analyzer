package LexicalAnalyzer;

public class Comment3State implements State {

   public boolean parse(Context context, String s) {
   		if (s.matches("[-]")) {
            Comment4State comment4State = new Comment4State();
            context.setState(comment4State);
            return false;
         }

         else {
            return false;
         }
   }

   public String toString(){
      return "Comment 3 State";
   }
}