package LexicalAnalyzer;

public class NumberState implements State {

   public boolean parse(Context context, String s) {
   		if(s.matches("[0-9]")){
   			context.setLexeme(context.getLexeme() + s);
   			return false;
   		}

         else if (s.matches("[.]")){
            context.setLexeme(context.getLexeme() + s);
            NumberDotState numberDotState = new NumberDotState();
            context.setState(numberDotState);
            return false;
         }

         else if (s.matches("[eE]")){
            context.setLexeme(context.getLexeme() + s);
            NumberEState numberEState = new NumberEState();
            context.setState(numberEState);
            return false;
         }
         
         else {
            return true;
         }
   }

   public String toString(){
      return "Number State";
   }
}