package LexicalAnalyzer;

import java.io.File;
import java.util.Scanner;

public class Program {
	Context context;
	String s, content;

	public Program(String textFilePath) {
		context = new Context();
		try {
			content = new Scanner(new File(textFilePath)).useDelimiter("\\Z").next();
		} catch (java.io.FileNotFoundException e) {
			content = textFilePath + "\n";
		}
	}

	public Token getToken() {
   		context.setTokenId(null);
   		context.setLexeme("");
   		context.setError("");
		context.setState(new StartState());

		int index = context.getIndex();
		s = content.substring(index, index + 1);

		while(!context.getState().parse(context, s)) {
			index ++;
			context.setIndex(index);
			s = content.substring(index, index + 1);
		}

      	Token t = new Token(context.getTokenId(), context.getLexeme());
      	if(!context.getError().equals("")) t.setError(context.getError());


		return t;
	}

	public Context getContext(){
		return context;
	}
}