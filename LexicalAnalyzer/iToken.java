package LexicalAnalyzer;

public interface iToken{
	public enum Id{TAGIDENT, IDENT, ENDTAGHEAD, PLUS, MINUS, MULT, DIVIDE, MODULO, EXP, LPAREN, RPAREN, EQUALS, LTHAN, GTHAN, COLON, COMMA, SCOLON, PERIOD, QUOTE, DQUOTE, NUMBER, EOF, COMMENT, LBRACK, RBRACK}
	public Id getId();
	public void setId(Id id);
	public String getLexeme();
	public void setLexeme(String lexeme);
}