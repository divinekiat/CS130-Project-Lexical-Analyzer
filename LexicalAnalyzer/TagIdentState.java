package LexicalAnalyzer;

public class TagIdentState implements State {

   public boolean parse(Context context, String s) {
   		if(s.matches("[a-zA-Z]")){
   			context.setLexeme(context.getLexeme() + s);
   			return false;
   		}
         else {
            return true;
         }
   }

   public String toString(){
      return "TagIdent State";
   }
}