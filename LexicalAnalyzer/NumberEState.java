package LexicalAnalyzer;

public class NumberEState implements State {

   public boolean parse(Context context, String s) {
   		if(s.matches("[0-9]")){
   			context.setLexeme(context.getLexeme() + s);
   			return false;
   		}

         else if ((context.getLexeme().matches(".*[eE]") || context.getLexeme().matches(".*[-]")) && (s.matches("[\"%()*+,/:;<=>]+") || s.matches("[a-zA-Z]"))){
            context.setLexeme(context.getLexeme() + s);
            context.setIndex(context.getIndex() + 1);
            context.setError("***lexical error: badly formed number\n");
            return true;
         }


         else if (context.getLexeme().matches(".*[eE]") && s.matches("[-]")){
            context.setLexeme(context.getLexeme() + s);
            return false;
         }

         else if (context.getLexeme().matches(".*[eE]") || context.getLexeme().matches(".*[-]")) {
            context.setError("***lexical error: badly formed number\n");
            return true;
         }

         else{
            return true;
         }
   }

   public String toString(){
      return "Number E State";
   }
}