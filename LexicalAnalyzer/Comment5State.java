package LexicalAnalyzer;

public class Comment5State implements State {

   public boolean parse(Context context, String s) {
   		if(s.matches("[>]")){
            context.setIndex(context.getIndex() + 1);
            return true;
   		}

         else {
            // go back
            Comment3State comment3State = new Comment3State();
            context.setState(comment3State);
            return false;
         }
   }

   public String toString(){
      return "Comment 5 State (-)";
   }
}