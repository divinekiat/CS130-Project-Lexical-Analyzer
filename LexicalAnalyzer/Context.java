package LexicalAnalyzer;

public class Context {
   private State state;
   private String lexeme, error;
   private Token.Id id;
   private int index = 0;

   public Context(){
      state = null;
   }

   public void setState(State state){
      this.state = state;		
   }

   public State getState(){
      return state;
   }

   public void setLexeme(String lexeme){
      this.lexeme = lexeme;
   }

   public String getLexeme(){
      return lexeme;
   }

   public void setTokenId(Token.Id id){
      this.id = id;
   }

   public Token.Id getTokenId(){
      return id;
   }

   public void setIndex(int index){
      this.index = index;
   }

   public int getIndex(){
      return index;
   }

   public String getError(){
      return error;
   }

   public void setError(String error){
      this.error = error;
   }


}