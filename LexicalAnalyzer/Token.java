package LexicalAnalyzer;

public class Token implements iToken{
	Id id = null;
	String lexeme = "";
	String error = "";

	public Token(Id id, String lexeme){
		this.id = id;
		this.lexeme = lexeme;
	}
	public Id getId(){
		return id;
	}
	public void setId(Id id){
		this.id = id;
	}

	public String getLexeme(){
		return lexeme;
	}

	public void setLexeme(String lexeme){
		this.lexeme = lexeme;
	}

	public String getError(){
		return error;
	}

	public void setError(String error){
		this.error = error;
	}

	public boolean isSame(Token t){
		if(getId() == t.getId() && getLexeme().equals(t.getLexeme()) && getError().equals(t.getError())) return true;

		return false;
	}
}