package LexicalAnalyzer;

public class Comment2State implements State {

   public boolean parse(Context context, String s) {
   		if (s.matches("[-]")) {
            context.setLexeme("");
            context.setTokenId(Token.Id.COMMENT);
            Comment3State comment3State = new Comment3State();
            context.setState(comment3State);

            return false;
   		} else {
            context.setIndex(context.getIndex() - 2);

            return true;
         }
   }

   public String toString() {
      return "Comment 2 State (-)";
   }
}