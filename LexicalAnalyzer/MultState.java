package LexicalAnalyzer;

public class MultState implements State {

   public boolean parse(Context context, String s) {
   		if(s.matches("[*]")){
   			context.setLexeme(context.getLexeme() + s);
            context.setTokenId(Token.Id.EXP);
            context.setIndex(context.getIndex() + 1);
            return true;
   		}

         else {
            return true;
         }
   }

   public String toString(){
      return "Mult State";
   }
}