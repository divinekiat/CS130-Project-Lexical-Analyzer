package LexicalAnalyzer;

public class StartState implements State {

   public boolean parse(Context context, String s) {
   		if(s.matches("[a-zA-Z]")){
   			context.setLexeme(context.getLexeme() + s);
   			IdentState identState = new IdentState();
   			context.setState(identState);
   			context.setTokenId(Token.Id.IDENT);
   			return false;
   		}

   		else if (s.matches("[0-9]")){
   			context.setLexeme(context.getLexeme() + s);
   		 	NumberState numberState = new NumberState();
   		 	context.setState(numberState);
   		 	context.setTokenId(Token.Id.NUMBER);
   		 	return false;
   		}

   		else if(s.matches("\\s+") || s.equals("\n")){
   			return false;
        }

   		else {
   			 switch (s){
   				case "<":
   					context.setLexeme(context.getLexeme() + s);
   		 			LThanState lThanState = new LThanState();
   		 			context.setState(lThanState);
   		 			context.setTokenId(Token.Id.LTHAN);
   		 			return false;
   				case ">":
   					context.setLexeme(context.getLexeme() + s);
   		 			context.setTokenId(Token.Id.GTHAN);
   		 			context.setIndex(context.getIndex() + 1);
   		 			return true;
               case "*":
                  context.setLexeme(context.getLexeme() + s);
                  MultState multState = new MultState();
                  context.setState(multState);
                  context.setTokenId(Token.Id.MULT);
                  return false;
               case "+":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.PLUS);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case "-":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.MINUS);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case "/":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.DIVIDE);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case "%":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.MODULO);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case "=":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.EQUALS);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case ":":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.COLON);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case ",":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.COMMA);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case ";":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.SCOLON);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case ".":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.PERIOD);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case "(":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.LPAREN);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case ")":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.RPAREN);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case "[":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.LBRACK);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case "]":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.RBRACK);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case "\'":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.QUOTE);
                  context.setIndex(context.getIndex() + 1);
                  return true;
               case "\"":
                  context.setLexeme(context.getLexeme() + s);
                  context.setTokenId(Token.Id.DQUOTE);
                  context.setIndex(context.getIndex() + 1);
                  return true;
   			}
            context.setError("***lexical error: illegal character (" + s + ")");
            context.setIndex(context.getIndex() + 1);
   			return true;
   		}
   }

   public String toString(){
      return "Start State";
   }
}