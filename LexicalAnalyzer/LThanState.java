package LexicalAnalyzer;

public class LThanState implements State {

   public boolean parse(Context context, String s) {
   		if(s.matches("[a-zA-Z]")){
   			context.setLexeme(context.getLexeme() + s);
            TagIdentState tagIdentState = new TagIdentState();
            context.setState(tagIdentState);
            context.setTokenId(Token.Id.TAGIDENT);
            return false;
   		}

         else if(s.matches("[/]")){
            context.setLexeme(context.getLexeme() + s);
            context.setTokenId(Token.Id.ENDTAGHEAD);
            context.setIndex(context.getIndex() + 1);
            return true;
         }

         else if(s.matches("[!]")){
            Comment1State comment1State = new Comment1State();
            context.setState(comment1State);
            return false;
         }

         else {
            return true;
         }
   }

   public String toString(){
      return "LThan State";
   }
}