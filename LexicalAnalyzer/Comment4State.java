package LexicalAnalyzer;

public class Comment4State implements State {

   public boolean parse(Context context, String s) {
   		if(s.matches("[-]")){
            Comment5State comment5State = new Comment5State();
            context.setState(comment5State);
            return false;
   		}

         else {
            Comment3State comment3State = new Comment3State();
            context.setState(comment3State);
            return false;
         }
   }

   public String toString(){
      return "Comment 4 State (-)";
   }
}