package LexicalAnalyzer;

public class Comment1State implements State {

   public boolean parse(Context context, String s) {
   		if (s.matches("[-]")) {
            Comment2State comment2State = new Comment2State();
            context.setState(comment2State);
            return false;
   		} else {
            context.setIndex(context.getIndex() - 1);
            return true;
         }
   }
   
   public String toString() {
      return "Comment 1 State (-)";
   }
}